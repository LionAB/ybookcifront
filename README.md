# YBOOK CI/CD
#### C3+C4 : Mettez en place la CI/CD de YBook
##### Énoncé

En autonomie, grâce aux notions abordées la séance précédente , mettez en place une CI/CD pour votre projet YBook (ou autre projet perso/pro), répondant aux cahier des charges définis ci-après

Cahier des charges :
- Hébergement backend/frontend sur GitLab (1 repository par personne, changez votre upstream)
- Répondant aux bonnes pratiques de l'intégration continue, abordées en cours
- Backend : au moins trois stages : build, test, deploy
- Frontend : au moins deux stages : build, deploy
- Utilisation d'une image Docker de votre container registry en guise d'image de build (fourniture du Dockerfile)
- Transit d'informations entre build et deploy, utilisation d'une stratégie Git adaptée
- Déploiement sur un environnement type VPS (ou machine locale), 1 container docker pour le backend, 1 container docker pour le frontend
- "Bonus" (Peut faire la différence entre bonne maîtrise et excellente maîtrise): 
    - Différencier le déploiement en fonction des environnements
    - Gestion des dépendances de jobs
    - Permettre un flux d'exécution différent en ciblant les pipelines planifiées
        Utilisation du cache


# Documentation FRONT Fantazoo
### C3+C4 : Mettez en place la CI/CD 
### ETAPES a faire
-   [ ] 1- Dockerfile
-   [ ] 2- Builder l'image
-   [ ] 3- Vérification du buildage
-  [ ] 4- Envoyer l'image sur Gitlab
-  [ ] 5- Créer des étapes dans le .gitlab-ci.yml (build,test,deploy)

2- C3+C4 

```
docker login registry.gitlab.com
docker build -t registry.gitlab.com/lionab/fantazoofront .
docker push registry.gitlab.com/lionab/fantazoofront
```

docker build -t ybookci/front:v0.1

docker run -d -p 80:80 \
-e REACT_APP_USER_POOL_ID=eu-west-3_Iekd8jDeb \
-e REACT_APP_AUTH_USER_POOL_WEB_CLIENT_ID=7llslfb09dtag2h7117og3ku72 \
ybookci/front:v0.1
## Contexte

Dans le contexte de notre transition numérique, le conseil d’administration a décidé
d’approuver le développement d’une web-app permettant aux étudiants de disposer
d’une plateforme de discussion commune. Cette application, YBook, devra s’interfacer
avec le système d’information de l’école et disposer de fonctionnalités techniques
détaillées dans ce cahier des charges.

## Fonctionnalités

Voici les fonctionnalités principales implémentées:

- Authentification: connection et inscription
- Page d'accueil / Fil d'actualité
- Page de profil
- Page des amis

## Tester l'application

1. Lancer le backend (se référer au README du repo correspondant)
2. Installer les dépendances:

```
npm install
```

3. Lancer l'application en local:

```
npm start
```
